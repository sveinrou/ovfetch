# OVfetch
Perl script that checks if OV is currently open.
Requires Perl 5, curl and iwgetid

#### OVfetch.pl
Fetches door status in two lines

<pre>
$ perl ovfetch.pl 
OV er åpent!	
</pre>

#### OVfetch_pro.pl
More verbose fetch that tells you how long OV has been closed.

<pre>
$ perl ovfetch_pro.pl 
OV er åpent!	

or

$ ./ovfetch_pro.pl 
OV er åpent!	
</pre>

